# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the astronciaiptv package.
#
# Serhii Kuznietsov <serhiy_k@meta.ua>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-10-20 12:03+0000\n"
"PO-Revision-Date: 2022-01-25 09:37+0000\n"
"Last-Translator: Serhii Kuznietsov <serhiy_k@meta.ua>\n"
"Language-Team: Ukrainian <kde-i18n-doc@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Poedit 2.4.2\n"

msgid "name"
msgstr "Українська"

msgid "m3u_ready"
msgstr "Готовий"

msgid "m3u_m3ueditor"
msgstr "Редактор плейлистів m3u"

msgid "m3u_noname"
msgstr "Без імені"

msgid "m3u_waschanged"
msgstr "Плейлист був змінений.<br>Ви хочете зберегти зміни?"

msgid "m3u_saveconfirm"
msgstr "Підтвердити збереження"

msgid "m3u_file"
msgstr "Готовий"

msgid "m3u_loadm3u"
msgstr "Відкрити плейлист M3U"

msgid "m3u_saveas"
msgstr "Зберегти як"

msgid "m3u_find"
msgstr "знайти"

msgid "m3u_replacewith"
msgstr "замінити"

msgid "m3u_replaceall"
msgstr "замінити все"

msgid "m3u_deleterow"
msgstr "видалити рядок"

msgid "m3u_addrow"
msgstr "додати рядок"

msgid "m3u_movedown"
msgstr "опустити вниз"

msgid "m3u_moveup"
msgstr "підняти вгору"

msgid "m3u_filtergroup"
msgstr "фільтр по групах (натисніть Enter)"

msgid "m3u_searchterm"
msgstr ""
"введіть пошуковий запит і натисніть Enter\n"
"праворуч оберіть групу для пошуку"

msgid "m3u_choosecolumn"
msgstr "оберіть стовпчик для пошуку"

msgid "m3u_openfile"
msgstr "Відкрити файл"

msgid "m3u_playlists"
msgstr "Плейлисти (*.m3u)"

msgid "m3u_loaded"
msgstr "завантажено"

msgid "m3u_savefile"
msgstr "Зберегти файл"

msgid "m3u_m3ufiles"
msgstr "M3U плейлисти (*.m3u, *.xspf)"

msgid "error"
msgstr "Помилка"

msgid "playerror"
msgstr "Помилка відтворення"

msgid "error2"
msgstr "Помилка Astroncia IPTV"

msgid "starting"
msgstr "запускається"

msgid "binarynotfound"
msgstr "Не знайдено бінарних модулів!"

msgid "nocacheplaylist"
msgstr "Кешування плейлиста відключене"

msgid "loadingplaylist"
msgstr "Йде завантаження плейлиста..."

msgid "playlistloaderror"
msgstr "Помилка завантаження плейлиста!"

msgid "playlistloaddone"
msgstr "Завантаження плейлиста завершено!"

msgid "cachingplaylist"
msgstr "Кешую плейлист..."

msgid "playlistcached"
msgstr "Кеш плейлиста збережено!"

msgid "usingcachedplaylist"
msgstr "Використовую кешований плейлист"

msgid "settings"
msgstr "Налаштування"

msgid "help"
msgstr "Допомога"

msgid "channelsettings"
msgstr "Налаштування каналу"

msgid "selectplaylist"
msgstr "Оберіть m3u плейлист"

msgid "selectepg"
msgstr "Оберіть файл телепрограми (XMLTV / JTV EPG)"

msgid "selectwritefolder"
msgstr "Оберіть папку для запису і знімків екрану"

msgid "deinterlace"
msgstr "Деінтерлейсинг"

msgid "useragent"
msgstr "User agent"

msgid "empty"
msgstr "Порожній"

msgid "channel"
msgstr "Канал"

msgid "savesettings"
msgstr "Зберегти налаштування"

msgid "save"
msgstr "Зберегти"

msgid "m3uplaylist"
msgstr "Плейлист M3U / XSPF"

msgid "updateatboot"
msgstr ""
"Оновлювати\n"
"при запуску"

msgid "epgaddress"
msgstr ""
"Адреса\n"
"телепрограми\n"
"(XMLTV / JTV)"

msgid "udpproxy"
msgstr "UDP проксі"

msgid "writefolder"
msgstr ""
"Папка для записів\n"
"та знімків екрану"

msgid "orselectyourprovider"
msgstr "Або оберіть провайдера"

msgid "resetchannelsettings"
msgstr "Скинути налаштування каналів і сортування"

msgid "notselected"
msgstr "не обрано"

msgid "close"
msgstr "Закрити"

msgid "nochannelselected"
msgstr "Не обрано канал"

msgid "pause"
msgstr "Пауза"

msgid "play"
msgstr "Відтворити"

msgid "exitfullscreen"
msgstr "Для виходу з повноекранного режиму натисніть клавішу"

msgid "volume"
msgstr "Гучність"

msgid "volumeoff"
msgstr "Гучність вимк."

msgid "select"
msgstr "Обрати"

msgid "tvguide"
msgstr "Телепрограма"

msgid "startrecording"
msgstr "Почати запис"

msgid "loading"
msgstr "Завантаження..."

msgid "doingscreenshot"
msgstr "Роблю знімок екрану..."

msgid "screenshotsaved"
msgstr "Знімок екрану збережено!"

msgid "screenshotsaveerror"
msgstr "Помилка створення знімку екрану!"

msgid "notvguideforchannel"
msgstr "Нема телепрограми для каналу"

msgid "preparingrecord"
msgstr "Підготовка запису"

msgid "nochannelselforrecord"
msgstr "Не обрано канал для запису"

msgid "stop"
msgstr "Стоп"

msgid "fullscreen"
msgstr "Повний екран"

msgid "openrecordingsfolder"
msgstr "Відкрити папку записів"

msgid "record"
msgstr "Запис"

msgid "screenshot"
msgstr "Знімок екрану"

msgid "prevchannel"
msgstr "Попередній канал"

msgid "nextchannel"
msgstr "Наступний канал"

msgid "tvguideupdating"
msgstr "Оновлення телепрограми..."

msgid "tvguideupdatingerror"
msgstr "Помилка оновлення телепрограми!"

msgid "tvguideupdatingdone"
msgstr "Оновлення телепрограми завершено!"

msgid "recordwaiting"
msgstr "Очікування запису"

msgid "allchannels"
msgstr "Усі канали"

msgid "favourite"
msgstr "Обране"

msgid "interfacelang"
msgstr "Мова програми"

msgid "tvguideoffset"
msgstr "Поправка часу"

msgid "hours"
msgstr "годин"

msgid "hwaccel"
msgstr ""
"Апаратне\n"
"прискорення"

msgid "sort"
msgstr ""
"Сортування\n"
"каналів"

msgid "sortitems1"
msgstr "як в плейлисті"

msgid "sortitems2"
msgstr "за алфавітом"

msgid "sortitems3"
msgstr "за зворотнім алфавітом"

msgid "sortitems4"
msgstr "власне сортування"

msgid "donotforgetsort"
msgstr ""
"Не забудьте вказати\n"
"власне сортування в налаштуваннях!"

msgid "moresettings"
msgstr "Більше налаштувань"

msgid "lesssettings"
msgstr "Менше налаштувань"

msgid "enabled"
msgstr "увімкнено"

msgid "disabled"
msgstr "вимкнено"

msgid "seconds"
msgstr "секунд."

msgid "cache"
msgstr "Кеш"

msgid "chansearch"
msgstr "Пошук по каналах"

msgid "reconnecting"
msgstr "Повторне підключення..."

msgid "group"
msgstr "Група"

msgid "hide"
msgstr "Сховати канал"

msgid "contrast"
msgstr "Контраст"

msgid "brightness"
msgstr "Яскравість"

msgid "hue"
msgstr "Відтінок"

msgid "saturation"
msgstr "Насиченість"

msgid "gamma"
msgstr "Гамма"

msgid "timeshift"
msgstr "Архів"

msgid "tab_main"
msgstr "Загальні"

msgid "tab_video"
msgstr "Відео"

msgid "tab_network"
msgstr "Мережа"

msgid "tab_other"
msgstr "Інше"

msgid "mpv_options"
msgstr "mpv опції"

msgid "donotupdateepg"
msgstr ""
"Не оновлювати\n"
"EPG під час запуску"

msgid "search"
msgstr "Пошук"

msgid "tab_gui"
msgstr "Інтерфейс"

msgid "epg_gui"
msgstr ""
"Вид\n"
"телепрограми"

msgid "classic"
msgstr "Класичний"

msgid "simple"
msgstr "Спрощений"

msgid "simple_noicons"
msgstr "Спрощений (без логотипів)"

msgid "update"
msgstr "Оновити"

msgid "scheduler"
msgstr "Планувальник запису"

msgid "choosechannel"
msgstr "Оберіть канал"

msgid "bitrate1"
msgstr "біт/с"

msgid "bitrate2"
msgstr "кбіт/с"

msgid "bitrate3"
msgstr "Мбіт/с"

msgid "bitrate4"
msgstr "Гбіт/с"

msgid "bitrate5"
msgstr "Тбіт/с"

msgid "starttime"
msgstr "Початок запису"

msgid "endtime"
msgstr "Завершення запису"

msgid "addrecord"
msgstr "Додати"

msgid "delrecord"
msgstr "Видалити"

msgid "plannedrec"
msgstr "Заплановані записи"

msgid "warningstr"
msgstr "Одночасний запис двох каналів недоступний!"

msgid "status"
msgstr "Статус"

msgid "recnothing"
msgstr "Нема запланованих записів"

msgid "recwaiting"
msgstr "Очікування початку запису"

msgid "recrecording"
msgstr "Йде запис"

msgid "activerec"
msgstr "Активні записи"

msgid "page"
msgstr "Сторінка"

msgid "fasterview"
msgstr ""
"Для швидшого завантаження каналів встановіть\n"
"кеш 1 або більше секунд в налаштуваннях мережі"

msgid "playlists"
msgstr "Плейлисти"

msgid "provselect"
msgstr "Обрати"

msgid "provadd"
msgstr "Додати"

msgid "provedit"
msgstr "Редагувати"

msgid "provdelete"
msgstr "Видалити"

msgid "provname"
msgstr "Назва"

msgid "of"
msgstr "з"

msgid "importhypnotix"
msgstr "Імпортувати з Hypnotix"

msgid "channelsonpage"
msgstr ""
"Каналів на\n"
"сторінку"

msgid "resetdefplaylists"
msgstr "Відновити типові"

msgid "license"
msgstr "Ліцензія"

msgid "openprevchan"
msgstr ""
"Під час запуску вмикати\n"
"попередній канал"

msgid "smscheduler"
msgstr "Планувальник"

msgid "praction"
msgstr ""
"Після закінчення\n"
"запису зробити:"

msgid "nothingtodo"
msgstr "нічого не робити"

msgid "stoppress"
msgstr "натиснути на Стоп"

msgid "turnoffpc"
msgstr "вимкнути ПК"

msgid "exitprogram"
msgstr "вийти з програми"

msgid "nohypnotixpf"
msgstr "Плейлисти Hypnotix не знайдені!"

msgid "theme"
msgstr "Тема"

msgid "videoaspect"
msgstr "Співвідношення сторін"

msgid "default"
msgstr "Типово"

msgid "zoom"
msgstr "Змінити масштаб"

msgid "openexternal"
msgstr "Відкрити в іншому програвачі"

msgid "open"
msgstr "Відкрити"

msgid "remembervol"
msgstr "Запам'ятати гучність"

msgid "hidempv"
msgstr "Приховати панель плеєра mpv"

msgid "filepath"
msgstr "Шлях до файла або посилання"

msgid "panscan"
msgstr ""
"Панорамування\n"
"та сканування"

msgid "tab_exp"
msgstr "Експериментальні"

msgid "exp1"
msgstr ""
"Регулювання бокової\n"
"плаваючої панелі"

msgid "exp2"
msgstr ""
"Ширина плейлиста в \n"
"повноекранному режимі"

msgid "exp3"
msgstr ""
"Висота плейлиста в \n"
"повноекранному режимі"

msgid "expwarning"
msgstr ""
"Ці налаштування\n"
"можуть бути нестабільними!"

msgid "mouseswitchchannels"
msgstr ""
"Перемикати канали\n"
"коліщатком миші"

msgid "actions"
msgstr "Дії"

msgid "defaultchangevol"
msgstr ""
"типово:\n"
"зміна гучності"

msgid "showplaylistmouse"
msgstr ""
"Показувати плейлист\n"
"під час руху миші"

msgid "showcontrolsmouse"
msgstr ""
"Показувати панель керування\n"
"під час руху миші"

msgid "volumeshort"
msgstr "Гучн."

msgid "hidetvguide"
msgstr "Приховати телепрограму"

msgid "flpopacity"
msgstr "Прозорість плаваючих панелей"

msgid "showhideplaylist"
msgstr "Показати/сховати плейлист"

msgid "showhidectrlpanel"
msgstr "Показати/сховати панель керування"

msgid "mininterface"
msgstr "Мінімальний інтерфейс"

msgid "panelposition"
msgstr ""
"Розташування\n"
"плаваючої панелі"

msgid "left"
msgstr "Ліворуч"

msgid "right"
msgstr "Праворуч"

msgid "doscreenshotsvia"
msgstr "Програма для знімків екрану"

msgid "playlistsep"
msgstr "Плейлист в окремому вікні"

msgid "volumechangestep"
msgstr "Крок регулювання гучності"

msgid "hideepgpercentage"
msgstr "Приховати проценти телепрограми"

msgid "xtreamnoconn"
msgstr "Неможливо під'єднатися"

msgid "procerror"
msgstr "Помилка обробки"

msgid "username"
msgstr "Логін"

msgid "password"
msgstr "Пароль"

msgid "url"
msgstr "URL адреса"

msgid "multipleplnote"
msgstr ""
"Ви можете додати декілька плейлистів\n"
"кнопкою Провайдери (після збереження налаштувань)"

msgid "favoritesplaylistsep"
msgstr "Обране (окремий плейлист)"

msgid "nochannels"
msgstr "Нема каналів"

msgid "epgname"
msgstr "EPG назва"

msgid "epgid"
msgstr "EPG id"

msgid "iaepgmatch"
msgstr "Неточне співставлення телепрограми"

msgid "using"
msgstr "Використовується"

msgid "Aspect"
msgstr "Співвідношення сторін"

msgid "Average Bitrate"
msgstr "Середній бітрейт"

msgid "Channel Count"
msgstr "Кількість каналів"

msgid "Channels"
msgstr "Канали"

msgid "Codec"
msgstr "Кодек"

msgid "colour"
msgstr "Колір"

msgid "Dimensions"
msgstr "Розміри"

msgid "Format"
msgstr "Формат"

msgid "Gamma"
msgstr "Гамма"

msgid "general"
msgstr "Загальні"

msgid "layout"
msgstr "Компонування"

msgid "Pixel Format"
msgstr "Формат пікселів"

msgid "Sample Rate"
msgstr "Частота дискретизації"

msgid "Stream Information"
msgstr "Інформація про потік"

msgid "Video"
msgstr "Відео"

msgid "Audio"
msgstr "Звук"

msgid "checkforupdates"
msgstr "Перевірити оновлення"

msgid "gotlatestversion"
msgstr "У вас вже встановлена найновіша версія!"

msgid "newversiongetfail"
msgstr "Не вдалося отримати інформацію про останню версію"

msgid "newversionavail"
msgstr "Доступна нова версія. Відвідати веб-сторінку для завантаження?"

msgid "aboutqt"
msgstr "Про Qt"

msgid "applog"
msgstr "Журнал Astroncia IPTV"

msgid "mpvlog"
msgstr "Журнал mpv"

msgid "copytoclipboard"
msgstr "Копіювати в буфер обміну"

msgid "choosesavefilename"
msgstr "Введіть назву файлу для збереження"

msgid "logs"
msgstr "Журнали"

msgid "Bits Per Pixel"
msgstr "Біт на один піксель"

msgid "epgloading"
msgstr "Телепрограма завантажується..."

msgid "unknownencoding"
msgstr ""
"Не вдалося завантажити плейлист: невідоме кодування! Будь ласка, "
"використовуйте плейлисти в кодуванні UTF-8."

msgid "menubar_exit"
msgstr "&Вихід"

msgid "menubar_title_file"
msgstr "&Файл"

msgid "menubar_title_play"
msgstr "&Відтворення"

msgid "menubar_playpause"
msgstr "&Відтворити / Призупинити"

msgid "menubar_stop"
msgstr "&Стоп"

msgid "minutes_plural"
msgid_plural ""
msgstr[0] "хвилина"
msgstr[1] ""
msgstr[2] ""

msgid "seconds_plural"
msgid_plural ""
msgstr[0] "секунда"
msgstr[1] ""
msgstr[2] ""

msgid "menubar_previous"
msgstr "&Попередній"

msgid "menubar_next"
msgstr "&Наступний"

msgid "speed"
msgstr "Швидкість"

msgid "menubar_normalspeed"
msgstr "&Нормальна швидкість"

msgid "menubar_video"
msgstr "В&ідео"

msgid "menubar_audio"
msgstr "&Аудіо"

msgid "menubar_fullscreen"
msgstr "На весь &екран"

msgid "menubar_compactmode"
msgstr "&Компактний режим"

msgid "menubar_csforchannel"
msgstr "&Настройки для каналу"

msgid "menubar_view"
msgstr "Ви&д"

msgid "menubar_options"
msgstr "&Параметри"

msgid "menubar_settings"
msgstr "&Налаштування"

msgid "menubar_about"
msgstr "П&ро Astroncia IPTV"

msgid "menubar_help"
msgstr "Д&овідка"

msgid "menubar_screenshot"
msgstr "&Знімок екрана"

msgid "menubar_mute"
msgstr "&Приглушити звук"

msgid "menubar_track"
msgstr "Дорі&жка"

msgid "empty_sm"
msgstr "порожньо"

msgid "menubar_checkupdates"
msgstr "&Перевірити оновлення"

msgid "ffmpeg_processing"
msgstr "Обробка запису..."

msgid "menubar_volumeminus"
msgstr "Г&учність -"

msgid "menubar_volumeplus"
msgstr "Гу&чність +"

msgid "list"
msgstr "список"

msgid "menubar_m3ueditor"
msgstr "&Редактор плейлистів m3u"

msgid "menubar_playlists"
msgstr "&Плейлисти"

msgid "menubar_channelsort"
msgstr "&Сортування каналів"

msgid "menubar_filters"
msgstr "&Фільтри"

msgid "menubar_postproc"
msgstr "&Подальша обробка"

msgid "menubar_deblock"
msgstr "&Розмивати квадратики"

msgid "menubar_dering"
msgstr "Видалити кіль&цеподібні артефакти"

msgid "menubar_debanding"
msgstr "Забрати &сходинковість (gradfun)"

msgid "menubar_noise"
msgstr "Додати &шуми"

msgid "menubar_black"
msgstr "Додати &чорні смуги"

msgid "menubar_phase"
msgstr "&Автовизначення фази"

msgid "menubar_extrastereo"
msgstr "&Розширене стерео"

msgid "menubar_karaoke"
msgstr "&Караоке (приглушення голосу)"

msgid "menubar_earvax"
msgstr "&Оптимізація для навушників (earvax)"

msgid "menubar_volnorm"
msgstr "&Нормалізація гучності"

msgid "errorvfapply"
msgstr "Помилка застосування фільтрів"

msgid "menubar_softscaling"
msgstr "Масштабувати про&грамно"

msgid "menubar_updateepg"
msgstr "&Оновити телепрограму"

msgid "playername"
msgstr "IPTV player"

msgid "hidebitrateinfo"
msgstr "Приховати інформацію про бітрейт / відео"

msgid "movedragging"
msgstr "Пересувати вікно мишкою"

msgid "playlist"
msgstr "список відтворення"

msgid "ignoringmpvoptions"
msgstr "Ігноруються невірні опції MPV"

msgid "showonlychplaylist"
msgstr "Показувати лише канали з плейлиста"

msgid "channels"
msgstr "каналів"

msgid "hideplaylistleftclk"
msgstr "Приховувати плейлист лівим кліком"

msgid "styleredefoff"
msgstr "Увімкнути переназначення стилів"

msgid "alwaysontop"
msgstr "Вікно завжди вгорі"

msgid "expfunctionwarning"
msgstr "УВАГА! Експериментальна функція, працює з проблемами"

msgid "menubar_expfunction"
msgstr "експериментальна функція"

msgid "autoreconnection"
msgstr ""
"Автоматичне\n"
"перепідключення"

msgid "customua"
msgstr "- користувацький -"

msgid "deletefromfav"
msgstr "Видалити з обраного"

msgid "scrrecnosubfolders"
msgstr ""
"Не створювати підтеки\n"
"для знімків екрану і записів"

msgid "helptext"
msgstr ""
"Astroncia IPTV, версія {}\n"
"\n"
"© 2021-2022 Astroncia\n"
"https://gitlab.com/muzena/iptv\n"
"\n"
"Підтримка:\n"
"https://unixforum.org/viewtopic.php?f=3&t=151801\n"
"\n"
"Багатоплатформовий плеєр\n"
"для перегляду інтернет-телебачення\n"
"\n"
"Підтримується телепрограма (EPG)\n"
"тільки в форматах XMLTV и JTV!\n"
"\n"
"Не працює канал? Права кнопка миші відкриває налаштування каналу.\n"
"\n"
"Коли вказали групу каналу в налаштуваннях, перезапустіть програму."
